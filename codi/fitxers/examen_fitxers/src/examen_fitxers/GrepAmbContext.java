package examen_fitxers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class GrepAmbContext {
	public static void main(String[] args) {
		if (args.length == 2) {
			Path path = Paths.get(args[1]);
			if (Files.isRegularFile(path) && Files.isReadable(path)) {
				grep(args[0], args[1]);
			} else {
				System.err.println("No es pot accedir a " + args[1]);
			}
		} else {
			System.err.println("Sintaxi: GrepSimple cadena fitxer");
		}

	}

	public static void grep(String cadena, String fileName) {
		List<String> buffer = new LinkedList<String>();
		String linia;
		int pendents=0;
		try (BufferedReader lector = new BufferedReader(new FileReader(fileName))) {
			while ((linia = lector.readLine()) != null) {
				// ens quedem com a màxim amb 2 línies
				if (buffer.size()>2)
					buffer.remove(0);
				// si la línia conté la cadena
				if (linia.contains(cadena)) {
					// mostrem el buffer
					for (String s : buffer) {
						System.out.println(s);
					}
					// mostrem línia actual
					System.out.println(linia);
					// no volem línies ja mostrades al buffer
					buffer.clear();
					// volem mostrar almenys dues línies més
					pendents = 2;
				} else {
					pendents--;
					if (pendents>=0) {
						// mostrem línies pendents
						System.out.println(linia);
					} else {
						// guardem nova línia al buffer
						buffer.add(linia);
					}
					if (pendents==-3)
						System.out.println("--");
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("No s'ha pogut accedir al fitxer: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Error en la lectura: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
