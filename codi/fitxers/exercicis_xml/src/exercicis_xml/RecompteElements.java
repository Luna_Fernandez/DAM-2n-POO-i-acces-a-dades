package exercicis_xml;

import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class RecompteElements {
	public static void main(String[] args) {
		try {
			XMLReader processadorXML =
					XMLReaderFactory.createXMLReader();
			RecompteElementsHandler handler = new RecompteElementsHandler();
			processadorXML.setContentHandler(handler);
			InputSource fitxerXML = new InputSource("persones.xml");
			processadorXML.parse(fitxerXML);
		} catch (SAXException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
	}
}
