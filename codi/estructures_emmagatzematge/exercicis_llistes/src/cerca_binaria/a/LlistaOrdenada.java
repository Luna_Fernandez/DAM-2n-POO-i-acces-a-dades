package cerca_binaria.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LlistaOrdenada {
	private List<Integer> llista = new ArrayList<Integer>();
	
	public void add(Integer element) {
		int pos = 0;
		Integer n;
		Iterator<Integer> it = llista.iterator();
		boolean sortida = false;
		while (it.hasNext() && !sortida) {
			n = it.next();
			if (n >= element) {
				sortida = true;
			} else
				pos++;
		}
		llista.add(pos, element);
	}
	
	public Integer get(int index) {
		return llista.get(index);
	}
	
	public boolean remove(Integer element) {
		return llista.remove(element);
	}
	
	public int size() {
		return llista.size();
	}
	
	/*
	 * Avantatges i inconvenients:
	 * 
	 * ArrayList en respecte LinkedList:
	 *  - Operació get ràpida, cosa que ens permet no haver de reimplementar els Iterator
	 *  - Operació add i remove més lenta, perquè cal desplaçar els altres elements
	 *  - La cerca binària serà més ràpida, perquè la base serà l'operació get.
	 */
	
	/*
	 * Per què composició i no herència?
	 * 
	 * Perquè amb l'herència, un usuari podria accedir a tots els mètodes de List, cosa que li
	 * permetria, per exemple, afegir elements en qualsevol posició i això faria que la llista
	 * ja no estigués ordenada. Amb la composició, l'usuari només pot utilitzar els mètodes que
	 * nosaltres li posem a disposició, en aquest exemple només el get, l'add, el remove i el
	 * size, i aixó garantim que la nostra llista sempre estigui ordenada.
	 */
}
