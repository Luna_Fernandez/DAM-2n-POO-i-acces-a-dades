package puntcami;

public class Punt {
	private double x;
	private double y;
	
	public Punt(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double distancia(Punt p) {
		return Math.sqrt((x-p.x)*(x-p.x)+(y-p.y)*(y-p.y));
	}
	
	@Override
	public String toString() {
		return "("+x+", "+y+")";
	}
}
