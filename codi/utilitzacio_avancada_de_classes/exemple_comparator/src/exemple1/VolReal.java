package exemple1;

import java.time.ZonedDateTime;

public class VolReal {
	private ZonedDateTime sortida;
	private ZonedDateTime arribada;
	
	public VolReal(ZonedDateTime sortida, ZonedDateTime arribada) {
		this.sortida = sortida;
		this.arribada = arribada;
	}
	
	public ZonedDateTime getSortida() {
		return sortida;
	}
	
	public ZonedDateTime getArribada() {
		return arribada;
	}
}
