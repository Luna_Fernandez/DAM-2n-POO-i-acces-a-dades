package scrabble;

public class Casella implements Cloneable {
	// poso Character i no char per si en algun moment volem caselles buides (null)
	private Character lletra;
	private int multiplicadorLletra = 1;
	private int multiplicadorParaula = 1;
	
	public Casella(Character lletra) {
		this.lletra = lletra;
	}
	
	public Casella(Character lletra, int multiplicadorLletra, int multiplicadorParaula) {
		if (multiplicadorLletra < 1 || multiplicadorLletra > 3)
			throw new IllegalArgumentException("multiplicadorLletra pot ser 1, 2 o 3");
		if (multiplicadorParaula < 1 || multiplicadorParaula > 3)
			throw new IllegalArgumentException("multiplicadorParaula pot ser 1, 2 o 3");
		
		this.lletra = lletra;
		this.multiplicadorLletra = multiplicadorLletra;
		this.multiplicadorParaula = multiplicadorParaula;
	}
	
	public Character getLletra() {
		return lletra;
	}

	public int getMultiplicadorLletra() {
		return multiplicadorLletra;
	}

	public int getMultiplicadorParaula() {
		return multiplicadorParaula;
	}

	public void setLletra(Character lletra) {
		this.lletra = lletra;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
