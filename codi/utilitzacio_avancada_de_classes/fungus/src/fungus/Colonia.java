package fungus;

public class Colonia implements Comparable<Colonia> {
	private String nom;
	private int poblacio;
	
	public Colonia(String nom) {
		this.nom = nom;
	}
	
	public void augmentaPoblacio() {
		poblacio++;
	}
	
	public void disminueixPoblacio() {
		poblacio--;
	}
	
	public int getPoblacio() {
		return poblacio;
	}

	@Override
	public int compareTo(Colonia c) {
		return getPoblacio() - c.getPoblacio();
	}
	
	@Override
	public String toString() {
		return "Colònia "+nom+", població: "+poblacio;
	}
}
