package consultes_parametres.ex5;

import java.util.List;
import java.util.Scanner;

public class UserView {
	private static Scanner input = new Scanner(System.in);
	
	public int chooseStore(List<Store> stores) {
		boolean found = false;
		int selectedStoreId=-1;
		for (Store store : stores) {
			System.out.println("ID botiga: "+store.id+"\n Adreça: "+store.address);
		}
		while (!found) {
			System.out.println("\nSelecciona una botiga: ");
			if (input.hasNextInt()) {
				selectedStoreId = input.nextInt();
				input.nextLine();
				for (Store store : stores) {
					if (store.id == selectedStoreId)
						found = true;
				}
			} else {
				input.nextLine();
			}
			if(!found){
				System.out.println("No existeix una botiga amb aquest ID.");
			}
		}
		return selectedStoreId;
	}
	
	public int chooseItem(List<Inventory> items) {
		boolean ok=false;
		int id=-1;
		System.out.println("Tria un ítem per veure'n més informació: ");
		while (!ok) {
			if (input.hasNextInt()) {
				id = input.nextInt();
				for (Inventory item : items) {
					if (item.id == id)
						ok=true;
				}
			}
			input.nextLine();
			if (!ok) {
				System.out.println("ID incorrecte.");
			}
		}
		return id;
	}
	
	public String askForString(String info) {
		String s;
		System.out.println(info+": ");
		s = input.nextLine();
		return s;
	}
	
	public void showFilm(Film film) {
		System.out.println("Títol : " + film.title
		+ "\n Descripció: " + film.description + "\n Actors: "
		+ film.actors + "\n Idioma: " + film.language
		+ "\n Any producció: " + film.releaseYear);
	}
	
	public void showInventory(List<Inventory> items) {
		for (Inventory item : items) {
			System.out.print("Inventari: "+ item.id+"  Títol: " + item.title);
			if (item.dueDate != null)
				System.out.println("  Data de retorn: " + item.dueDate);
			else
				System.out.println("  Disponible");
		}
	}
	
	public SearchCriteria menu() {
		int option = -1;
		int nOptions = SearchCriteria.values().length;
		do{
			for (int i=0; i<nOptions; i++) {
				System.out.println((i+1)+". "+SearchCriteria.values()[i]);
			}
			if (input.hasNextInt()) {
				option = input.nextInt();
			}
			input.nextLine();
		} while(option<1 || option>nOptions);
		return SearchCriteria.values()[option-1];
	}
}
