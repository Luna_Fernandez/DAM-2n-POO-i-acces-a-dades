package consultes_estatiques;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * 3. Fes un programa que mostri quins ítems i de 
 * quines botigues té en lloguer el client de nom ALLISON STANLEY. 
 * També haurà de mostrar la data de retorn d'aquests ítems.
 */
public class Ex3 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		// consulta que mostra totes les pel·lícules llogades i ja retornades
		/*	"SELECT f.title, a.address, r.return_date FROM film f" +
			" JOIN inventory i USING(film_id)" +
			" JOIN rental r USING (inventory_id)" +
			" JOIN customer c USING(customer_id)" +
			" JOIN store s ON i.store_id=s.store_id" +
			" JOIN address a ON a.address_id=s.address_id" +
			" WHERE c.first_name LIKE 'ALLISON' AND c.last_name LIKE 'STANLEY'"*/
		// consulta que retorna les pel·lícules que están en lloguer ara i la seva data máxima de retorn
		String sql = "SELECT f.title, a.address, adddate(r.rental_date, f.rental_duration)" +
				" FROM film f" +
				" JOIN inventory i USING(film_id)" +
				" JOIN rental r USING (inventory_id)" +
				" JOIN customer c USING(customer_id)" +
				" JOIN store s ON i.store_id=s.store_id" +
				" JOIN address a ON a.address_id=s.address_id" +
				" WHERE r.return_date IS NULL" +
				" AND c.first_name LIKE 'ALLISON'" +
				" AND c.last_name LIKE 'STANLEY'";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(sql);) {
			while (rs.next())
				System.out.print("Títol: "+rs.getString(1)+"\n Address: "+rs.getString(2)+"\n ReturnDate: "+rs.getString(3)+"\n");
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
