### Mètodes i classes abstract

#### Classes abstract

D'una classe que es declara com a **abstract** no se'n poden crear
instàncies. Això es fa quan es considera que crear una classe és útil
per definir unes característiques comunes a una colla de classes que en
derivaran, però que el seu nivell d'abstracció és massa gran per crear
un objecte que no defineixi de quin subtipus és.

Per exemple, ens pot interessar crear la classe *Animal* que defineixi
les característiques i comportaments que tindrà qualsevol animal del
nostre programa, però pot no tenir sentit crear un objecte animal, sinó
que voldrem que es digui de quin animal es tracta: crear un objecte de
tipus *Gat* o *Elefant*.

En la majoria de casos, una classe abstracte tindrà un o més mètodes
abstractes, però no és obligatori.

#### Mètodes abstract

En una classe, un mètode es declara com a **abstract** quan es vol garantir
que totes les classes que en derivin tinguin aquest mètode, però no es
vol implementar un comportament per defecte. En un mètode abstracte es
declaren els tipus dels paràmetres i el valor de retorn, però no
s'inclou codi. Llavors, totes les classes que derivin de la classe que
té el mètode abstracte l'han d'implementar, o tornar-lo a declarar com a
*abstract*.

Per exemple, a la classe *Animal* anterior podríem tenir un mètode
*menja* abstracte, perquè considerem que tots els nostres animals
menjaran, però no podem definir un comportament per defecte que es
compleixi per la majoria. Si derivem *Mamifer* d'*Animal* podríem optar
per implementar el mètode *menja* a *Mamifer*, o bé tornar-lo a declarar
com a *abstract* i deixar que siguin les classes que deriven de
*Mamifer* les que ho implementin.

Una classe amb mètodes abstractes està a mig camí entre una classe i una
interfície: té alguns mètodes implementats i en té alguns altres que no,
com en les interfícies. Si ens trobem que una classe té tots els seus
mètodes abstractes, és millor que la implementem com una interfície.

Una classe que incorpora mètodes abstractes ha de ser sempre abstracte,
ja que no tindria sentit crear un objecte d'una classe a la qual li
manca la implementació d'algun dels seus mètodes.
