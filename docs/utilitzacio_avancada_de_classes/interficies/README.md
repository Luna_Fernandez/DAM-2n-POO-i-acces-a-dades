### Interfícies

Una **interfície** és similar a una classe, però no té codi que la
implementi, només les capçaleres dels mètodes i variables estàtiques
constants (*static final*). Una classe pot declarar que *implementa* una
interfície, i llavors cal omplir el codi que manca a aquesta classe (o
bé declarar els mètodes abstractes i obligar a que sigui una classe
derivada qui ho faci, com veurem més endavant).

***Les interfícies ens permeten garantir que una determinada classe
compleix un determinat contracte***, és a dir, que inclou alguns mètodes
concrets.

A més, les interfícies són una versió fluixa de l'herència múltiple, que
no existeix en Java. Es poden utilitzar quan volem donar
característiques comunes a diverses classes que no estan relacionades
entre elles per l'herència.

Una interfície pot ampliar una altra interfície, creant una estructura
similar a l'herència de classes. Per altra banda, una interfície pot no
contenir cap mètode, i llavors només serveix per indicar que una classe
compleix una determinada propietat.

A partir de la versió 8 de Java, algunes interfícies poden tenir alguns
mètodes amb codi. Això s'ha fet per poder afegir funcionalitat a algunes
interfícies i classes de les API sense que això trenqui la compatibilitat
de programes ja existents que tinguin classes que implementen alguna
d'aquestes interfícies.

En cas que una classe heredi d'una classe què té un mètode que també
existeix en una interfície que implementa i que, a més, té codi, el
codi que ve d'una classe sempre té preferència.

Per exemple:

![Exemple interfície java 8](docs/utilitzacio_avancada_de_classes/imatges/exemple_interficie_java8.png)

Anem a estudiar l'ús de les interfícies amb alguns exemples extrets de
les API del Java.

 * [La interfície Comparable](comparable.md)
 * [La interfície Cloneable](cloneable.md)
 * [Exercicis](exercicis.md)
