## Tasques habituals

### Obtenir els últims canvis del repositori original

- Baixem tota la informació nova del respositori:

```
$ git fetch upstream
```

El *fetch* crea una nova branca anomenada
*remotes/upstream/master* al nostre repositori local, amb el
contingut del repositori remot que hem anomenat *upstream*
durant la configuració inicial.

Per veure totes les branques, també les remotes, podem fer:

```
$ git branch -a
```

- Unim la branca upstream/master que hem obtingut a la
nostra branca master:

```
$ git merge upstream/master
```

- Si hem fet canvis directament a la web del GitLab, caldrà que
els baixem i els integrem també:

```
$ git pull
```

L'ordre *git pull* és una forma ràpida de fer un *fetch* i un
*merge*. Noteu que amb aquesta instrucció estem fent gairebé
el mateix que abans, però amb el nostre propi repositori a GitLab.

Un cop integrat tot en el nostre repositori local, ja ho
podem pujar tot de nou al GitLab:

```
$ git push
```

### Fer i entregar uns exercicis

Per entregar un conjunt d'exercicis és convenient treballar en una branca
específica que parteixi de l'estat del repositori original. D'aquesta manera
evitarem enviar tots els nostres canvis i exercicis anteriors cap al
respositori original cada vegada que volem entregar un exercici.

- Creem una branca nova a partir del contingut del repositori original:

```
$ git branch <nom nova branca> remotes/upstream/master
```

Hem creat la nova branca, però encara estem treballant a master. Per canviar
a la nova branca:

```
$ git checkout <nom nova branca>
```

Per poder canviar a aquesta nova branca cal abans haver guardat (commit) o
descartat tots els canvis que hi poguessin haver al nostre directori de
treball.

Podem comprovar que realment estem a la branca que volem amb:

```
$ git branch
```

Un cop a la nova branca podem fer els exercicis, i fer tants commits com
necessitem. Tot això es fa en local.

Quan hem acabat, hem de pujar la nova branca al nostre repositori del GitLab:

```
$ git push origin
```

Això crea la nova branca al GitLab. Per tal d'entregar definitivament els
exercicis anem a la web del GitLab i obrim un nou *merge request* de la
nostra branca nova cap a la branca master del repositori original.

A més d'això, probablement voldrem passar els canvis d'aquesta branca nova
a la nostra branca master, i evitar així tenir múltiples branques diferents,
una per cada bloc d'exercicis que volem entregar.

Per ajuntar la nova branca a la branca master ens canviem primer a la branca
master i després fem un merge de la branca nova:

```
$ git checkout master
$ git merge <nom nova branca>
```

Com sempre, podem pujar aquests canvis al GitLab amb:

```
$ git push
```

i si volem podem desfer-nos ja d'aquesta branca local:

```
$ git branch -D <nom nova branca>
```

Més endavant, podem entrar al GitLab i eliminar també la branca nova del
GitLab, però no ho hem de fer fins que els exercicis hagin estat revisats,
ja que es perdrà el *merge request* si eliminem la branca corresponent.

### Canviar de branca amb feina a mig fer

Sovint voldrem canviar de branca de treball perquè estem treballant en un
grup d'exercicis diferent a diferents branques.

Si tots els canvis que hem fet s'han guardat al git amb commit no hi ha cap
problema. Però de vegades tenim canvis a mig fer que no volem perdre, però que
tampoc volem guardar encara com un commit al repositori.

En aquests casos és molt útil l'ordre:

```
$ git stash
```

Aquesta ordre guarda una còpia de tot el que tenim al directori de treball.
A partir d'aquí podem canviar de branca sense por a perdre aquestes dades.

Quan més tard tornem a aquesta branca de treball podem recuperar el que
estàvem fent.

Podem veure totes les captures que tenim guardades:

```
$ git stash list
```

Cada captura tindrà un número, que podem recuperar amb:

```
$ git stash apply stash@{núm}
```

O amb:

```
$ git stash apply
```

si volem recuperar l'última captura que hem fet.

Un cop recuperades aquestes dades, segurament voldrem esborrar la còpia
amb:

```
$ git stash drop stash@{núm}
```

Finalment, si el que volem és recuperar l'última captura que hem fet i
després esborrar-la, ho podem fer tot amb una sola ordre:

```
$ git stash pop
```

### Resoldre un conflicte

Quan unim dues branques amb un *merge* pot passar que hi hagi una part
d'algun fitxer que s'hagi modificat a les dues branques. Aleshores el git
no sap com ha d'unir les dues versions i es produeix un conflicte.

A les següent seccions s'explica com es poden resoldre aquests conflictes,
però si ens atavalem i volem anul·lar el *merge* sempre ho podem fer amb:

```
$ git merge --abort
```

Cal tenir en compte que abortar un *merge* pot afectar als fitxers del
nostre directori de treball que no hagin estat guardats amb un *commit* o un
*stash*. Per això, sempre és recomanable **tenir el directori de treball
sense canvis per desar** abans de provar un *merge*.

#### Fitxers de text

Si el fitxer en conflicte és un fitxer de text, el git ens haurà afegit
uns indicadors al propi fitxer, i entre aquests indicadors les dues
versions en conflicte:

```
<<<<<<< branca_actual:fitxer
Part en conflicte del fitxer a la branca actual
=======
Part en conflicte del fitxer a la branca que s'està intgrant
>>>>>>> branca_a_integrar:fitxer
```

A partir d'aquesta informació podem quedar-nos amb les parts que volem i
eliminar les marques que ha afegit el git.

Un cop hem resol el conflicte li indiquem al git fent un *git add* del o
dels fitxers que tenien conflictes.

Finalment, acabarem el *merge* amb un *git commit*.

#### Fitxers binaris

En el cas de fitxers binaris, com imatges, no podem barrejar les dues
versions: o ens quedem amb la nostra versió de la branca actual (*ours*) o
amb la versió que ve de l'altra branca (*theirs*).

Si volem la versió de la nostra branca actual fem:

```
$ git checkout --ours -- fitxer_en_conflicte
```

I si volem la versió de la branca que estem integrant:

```
$ git checkout --theirs -- fitxer_en_conflicte
```

A partir d'aquí procedim com en el cas anterior: afegint el fitxer final
amb un *git add* i acabant l'operació de *merge* amb un *git commit*.
