## Introducció a MongoDB

El MongoDB és un SGBD documental. Això significa que, a diferència dels
sistemes relacionals més habituals, la unitat de treball del MongoDB és el
document.

Un document és, essencialment, una estructura de dades formada per parelles
de camps i valors. En el cas del MongoDB, els documents es codifiquen en
el format JSON.

Treballar amb documents ofereix una sèria d'avantatges i inconvenients
respecte a un SGBD relacional:

- Els documents tenen una representació directa en molts llenguatges de
programació. Això significa que la traducció entre les dades d'una BD i els
objectes a memòria és molt més senzilla que en una base de dades relacional.

    En el cas del format JSON, el Javascript pot interpretar els documents
    com si fóssin objectes directament. Altres llenguatges tenen estructures
    que faciliten el tractament de parelles camp-valor, com els diccionaris.

- Els documents permeten emmagatzemar les dades en estructures que són
impossibles en una BD relacional. Per exemple, un camp pot contenir un
array com a valor (i ésser per tant una propietat multivaluada, concepte que
no es pot implementar en el model relacional). O un document en format d'arbre
pot contenir molta informació niuada, cosa que evita l'ús extensiu de *joins*.

    Les BD relacionals han tingut (i tenen) molt èxit perquè es basen en un
    model que permeten donar resposta teòricament a qualsevol consulta. Però
    la fragmentació de dades en moltes taules obliga a crear consultes molt
    complexes i poc eficients, en què s'han d'unir moltes taules i, per tant,
    consultar molts registres, per donar resposta a consultes habituals.

    En canvi, en els BD documentals, el format dels documents no es dissenya
    genèricament, sinó que s'adapta a les necessitats concretes de l'aplicació
    que es vol desenvolupar, de manera que les consultes més habituals es
    puguin resoldre consultant únicament un document.

    Això fa que el rendiment d'un sistema documental pugui ser molt alt si
    els documents s'han dissenyat bé i s'han creat els índexos adequats, tot i
    que algunes consultes complexes que impliquin diversos documents siguin
    més lentes que en un sistema relacional.

- Contràriament al sistema relacional, els documents permeten una gran
variabilitat en les dades. Per exemple, podem tenir documents on apareguin
camps que en d'altres documents no hi siguin, i documents amb estructures
molt diferents entre ells emmagatzemats a la mateixa col·lecció. Fins i tot,
els documents es poden modificar i afegir o treure camps dinàmicament.

    Això fa idoni els sistemes documentals per aquelles aplicacions en què
    la diversitat de les dades a tractar complica la creació d'un bon
    model relacional.

### Instal·lació

La instal·lació bàsica de MongoDB és molt senzilla.

Per Debian es poden seguir
[aquestes instruccions](https://docs.mongodb.org/master/tutorial/install-mongodb-on-debian/).
Amb això afegirem un nou repositori des d'on podrem baixar l'última versió
del servidor de MongoDB. Cal utilitzar el paquet per Debian 7, que també
funciona per Debian 8.

També podem utilitzar el paquet proporcionat oficialment per Debian, encara que
serà d'una versió anterior.

Per Windows hem de seguir
[aquestes instruccions](https://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/).
Cal obtenir un instal·lador adequat a la versió del sistema.
