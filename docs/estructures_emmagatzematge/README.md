**POO i accés a dades**

M3UF5. POO. Biblioteques de classes fonamentals
===============================================

Aplicació de les estructures d'emmagatzematge en la POO
---------------------------------------------------------

Una **estructura dinàmica** de dades és aquella que veu modificada la seva
estructura durant l'execució del programa, en oposició a les estructures
estàtiques, que sempre tenen el mateix nombre d'elements organitzats de
la mateixa manera. En algunes estructures dinàmiques només es varia la
quantitat d'elements que emmagatzemen, mentre que en d'altres es
modifica fins i tot la relació entre aquests elements.

En la major de part dels programes no es coneix la quantitat d'elements
a tractar abans de la seva execució, cosa que implica que s'ha
d'utilitzar memòria dinàmica per a poder-los gestionar. Si a més, la
quantitat d'elements pot variar durant l'execució del programa i no és
fàcil establir-ne un màxim, o aquest màxim és molt superior als valors
habituals, es fa necessari l'ús d'estructures dinàmiques.

Afortunadament, a les API de Java ja hi ha implementades la major part
d'estructures dinàmiques d'ús habitual, cosa que ens permetrà
centrar-nos en el seu ús.

 * [Interfícies, implementacions i algorismes](collections_framework/README.md)
 * [Seqüències](sequencies/README.md)
 * [Altres estructures de dades](altres/README.md)
 * [Exercicis](exercicis/README.md)
